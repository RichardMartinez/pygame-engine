import games
import color


# class MoverTest(games.Polygon, games.Mover):
#     pass


screen = games.Screen(width=600, height=600)

space_bg = games.load_image("test_assets/background-black.png")
player_img = games.load_image("test_assets/player.png")
player_img = games.scale_image(player_img, 3)

screen.set_background(space_bg)

# blue_circle = games.Circle(screen, 300, 300, 150, color.BLUE)
# red_circle = games.Circle(screen, 300, 300, 100, color.RED)
# yellow_circle = games.Circle(screen, 300, 300, 50, color.YELLOW)
# text_box = games.Text(screen, 300, 300, "Test Text Box", size=55, color=color.PURPLE)
# player_sprite = games.Sprite(screen, 300, 300, player_img)

# shape = [(0, 0), (0, 50), (50, 50), (50, 0)]
# polygon = games.Polygon(screen, 300, 300, shape, color=color.RED, outline=color.BLUE, thickness=10)

# timer = games.Timer(interval=100)
# screen.add_object(timer)

# mover_test = MoverTest(screen, 300, 300, shape, color=color.RED, outline=color.BLUE, thickness=10)
# mover_test.init_mover(1, 0, 1)

# msg = games.Message(screen, 300, 300, "Test Message on the Screen", size=40,
#                     color=color.RED, lifetime=50*5, after_death=lambda: print("Done"))

# death_sfx = games.load_sound("test_assets/Death.wav")
# death_sfx.play()

# coin_sfx = games.load_sound("test_assets/Pickup_Coin.wav")
# coin_sfx.play(-1)

# meadows_song = games.load_sound("test_assets/meadows.wav")
# meadows_song.play(-1)

# sprite = games.Sprite(screen, 300, 300, player_img)

# empty_surface = games.pygame.Surface((50, 50)).convert()
#
#
# def color_surface(color):
#     new_surface = empty_surface.copy()
#     new_surface.fill(color)
#     return new_surface


# red_surface = color_surface(color.RED)
# blue_surface = color_surface(color.BLUE)
# yellow_surface = color_surface(color.YELLOW)
# green_surface = color_surface(color.GREEN)
# orange_surface = color_surface(color.ORANGE)
# purple_surface = color_surface(color.PURPLE)
#
# nonrepeating = [red_surface, blue_surface, yellow_surface]
# repeating = [green_surface, orange_surface, purple_surface]
#
# animation = games.Animation(screen, 300, 300, nonrepeating, repeating, repeat_interval=50, n_repeats=9)

def test_assetify(lst):
    return [f"test_assets/{string}" for string in lst]


nonrepeating = ["pixel_ship_yellow.png", "pixel_ship_red_small.png", "pixel_ship_green_small.png",
                "pixel_ship_blue_small.png"]
repeating = ["pixel_laser_yellow.png", "pixel_laser_red.png", "pixel_laser_green.png",
             "pixel_laser_blue.png"]

nonrepeating = test_assetify(nonrepeating)
repeating = test_assetify(repeating)

nonrepeating, repeating = games.load_animation(nonrepeating, repeating)

animation = games.Animation(screen, 300, 300, nonrepeating, repeating, repeat_interval=50, n_repeats=4+4*2)

screen.mainloop()