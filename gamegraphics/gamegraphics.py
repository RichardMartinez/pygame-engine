"""
Pygame port of John Zelle's graphics.py.
This description may be expanded later.

Developed by: Richard Martinez
"""

# This is a ported version of John Zelle's graphics.py. It is currently unfinished. To test moving shapes,
# use left and right click to move left and right, and middle click to move down.


import pygame
import sys
import math  # Only used in Line class to find angle which never worked
from gamegraphics.colors import Color
pygame.init()
pygame.font.init()


def color_rgb(r, g, b):
    """r,g,b are intensities of red, green, and blue in range(256)
    Returns color specifier string for the resulting color"""
    return "#%02x%02x%02x" % (r, g, b)


def assign_color(obj, attr, var, to_dict):
    """Easy assignment of colors to different vars.
    Supports 4 types:
    None (Default Color), str (Color.str),
    tuple (Custom RGB value), Color.attr (pure Color attr)"""
    if var is not None:
        if len(var) == 1:
            var = var[0]

    var_type = type(var)
    if not to_dict:
        if var is None:
            setattr(obj, attr, obj.DEFAULT_CONFIG[attr])
        elif var_type is str:
            setattr(obj, attr, getattr(Color, var.upper()))
        else:  # either tuple or pure Color obj
            setattr(obj, attr, var)
    else:
        if var is None:
            getattr(obj, "config")[attr] = obj.DEFAULT_CONFIG[attr]
        elif var_type is str:
            getattr(obj, "config")[attr] = getattr(Color, var.upper())
        else:  # either tuple or pure Color obj
            getattr(obj, "config")[attr] = var


class GraphicsError(Exception):
    """Generic error class for graphics module exceptions."""
    @staticmethod
    def assertType(obj, type_wanted, msg):
        if type(obj) is not type_wanted:
            raise GraphicsError(msg)


# class Transform:
#     pass


# Currently missing methods: setCoords, plotPixel, get/checkMouse, get/checkKey, toScreen/World, setMouseHandler
class GraphWin:
    """A GraphWin is a toplevel window for displaying graphics."""
    DEFAULT_CONFIG = {"bg_color": Color.TKINTERBG}

    # What is autoflush? It doesn't seem to have any change at all in original graphics.
    def __init__(self, title="Graphics Window", width=200, height=200, bg_color=None):
        GraphicsError.assertType(title, str, "Title must be a string.")

        self.title = title
        self.width = width
        self.height = height

        self.bg_color = None
        assign_color(self, "bg_color", bg_color, to_dict=False)

        self.window = pygame.display.set_mode((self.width, self.height))
        pygame.display.set_caption(self.title)

        self.center_pos = (self.width // 2, self.height // 2)

        self.items = []

        self.running = False
        self.clock = pygame.time.Clock()
        self.FPS = 60

    def __repr__(self):
        return f"GraphWin('{self.title}', {self.getWidth()}, {self.getHeight()})"

    def mainloop(self):
        """Main loop for the window, pygame relies on a while loop so it should go in here."""
        # PASS COMMAND ARGUMENT FOR CUSTOM WHILE LOOP??
        self.running = True
        while self.running:
            self.clock.tick(self.FPS)
            self.__check_events()

            self.__redraw_window()

    def __check_events(self):
        """Checks all user events and acts accordingly. Uses pygame event system. For internal use only."""
        # POTENTIALLY MOVE TO CUSTOM COMMAND ARG OF MAINLOOP
        for event in pygame.event.get():
            # if type(self.items[0]) is Entry:
            #     text = self.items[0].event_handler(event)
            #     if text:
            #         print(text)

            if event.type == pygame.QUIT:
                self.close()

            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    self.items[0].move(10, 0)
                elif event.button == 2:
                    self.items[0].move(0, 10)
                elif event.button == 3:
                    self.items[0].move(-10, 0)
                # elif event.button == 2:
                #     self.items = []

    def __redraw_window(self):
        """Redraws the window with all items. For internal use only."""
        self.window.fill(self.bg_color)

        for item in self.items:
            if type(item) is not tuple:
                item._draw(self.window)
            else:  # Filter out only pygame.Surfaces  # if type(item[0]) is pygame.Surface:
                self._drawSurface(item[0], item[1])

        pygame.display.update()

    @staticmethod
    def close():
        """Ends pygame and closes the window."""
        pygame.quit()
        sys.exit()

    ##### HERE STARTS MAYBE REDUNDANT METHODS #####
    # These methods don't seem to have a purpose in this version but are here to avoid call errors.

    # isClosed/Open seems kind of redundant in pygame, since program needs to be running to use either and will
    # therefore always return either True or False. May be removed if proven not necessary
    def isClosed(self):
        return not self.running
        # Will always return False?

    def isOpen(self):
        return self.running
        # Will always return True?

    def flush(self):
        pass
        # Again this has to do with autoflush which I don't think makes a difference.

    def redraw(self):
        """Reformats list of items and redraws the window/"""
        for item in self.items[:]:
            item.undraw(self)
            item.draw(self)
        self.__redraw_window()
        # Kinda weird but ok. Also access to double protected function __redraw_window may cause issues.

    ##### HERE ENDS MAYBE REDUNDANT METHODS #####

    def plot(self, x, y, *color):
        """Draws a point on the screen."""
        point = Point(x, y)
        point.setFill(*color)
        point.draw(self)
        return point

    def addItem(self, item):
        """Adds an item to the screen to be drawn."""
        self.items.append(item)

    def delItem(self, item):
        """Removes an item from the screen so it will no longer be drawn."""
        self.items.remove(item)

    def getHeight(self):
        """Return the height of the window"""
        return self.height

    def getWidth(self):
        """Return the width of the window"""
        return self.width

    def setBackground(self, *bg_color):
        """Set background color of the window."""
        assign_color(self, "bg_color", bg_color, to_dict=False)

    def blit(self, surface, pos):
        """Allow for drawing of pygame.Surfaces not just native item classes."""
        self.items.append((surface, pos))

    def _drawSurface(self, surface, pos):
        """Actual blit call for drawing pygame.Surfaces. For internal use only."""
        self.window.blit(surface, pos)

    def surfCenter(self, surface):
        """Return a point at the center of a given surface drawn to the graphwin."""
        pos = None
        for item in self.items:
            if type(item) is tuple:
                if item[0] == surface:
                    pos = item[1]
        if pos is None:
            raise GraphicsError("Given surface not drawn to the screen.")

        surfRect = surface.get_rect(topleft=pos)
        return Point(*surfRect.center)


# REMOVE REDUNDANT METHODS TO AVOID ERRORS?
class GraphicsObject:
    """Generic base class for all of the drawable objects"""
    DEFAULT_CONFIG = {"fill": None,
                      "outline": Color.BLACK,
                      "width": 1,
                      "arrow": "none",
                      "text": "",
                      "font": {"name": "arial", "size": 12, "bold": False, "italic": False, "custom": False}
                      }

    def __init__(self, options):
        # config is the dictionary of configuration options for the widget.
        config = {}
        for option in options:
            config[option] = self.DEFAULT_CONFIG[option]
        self.config = config

        # HERE TO AVOID POTENTIAL ERRORS. REMOVE LATER?
        self.p1, self.p2 = None, None
        self.x, self.y = 0, 0
        self.center_point = None
        self.center = (0, 0)
        self.radius = 0
        self.p1Draw, self.p2Draw = (0, 0), (0, 0)
        self.points = []

    def draw(self, graphwin):
        """Draw the object in graphwin, which should be a GraphWin object.
        Actual draw call should be _draw."""
        graphwin.addItem(self)

    def undraw(self, graphwin):
        """Undraws the object from graphwin."""
        graphwin.delItem(self)

    def setFill(self, *color):
        assign_color(self, "fill", color, to_dict=True)

    def setOutline(self, *color):
        assign_color(self, "outline", color, to_dict=True)

    def setWidth(self, n):
        """Set the border padding width to n pixels."""
        self.config["width"] = n
        self._update()

    def _update(self):
        pass

    def move(self, dx, dy):
        """Moves the object by the given dx, dy."""
        self_type = type(self)
        if self_type in [Rectangle, Oval, Circle, Line]:
            self.p1.move(dx, dy)
            self.p2.move(dx, dy)
            self.getCenter().move(dx, dy)
        elif self_type in [Polygon]:
            for p in self.points:
                p.move(dx, dy)
            self.drawPoints = tuple(p.getCenter() for p in self.points)
        elif self_type in [Text, Image]:
            self.anchor.move(dx, dy)
        elif self_type in [Point]:
            self.x += dx
            self.y += dy
            self.center = (self.x, self.y)
        self._update()

    def clone(self):
        """Return an exact copy of the object."""
        args = ()
        self_type = type(self)
        if self_type in [Rectangle, Oval, Line]:
            args = (self.p1, self.p2)
        elif self_type in [Circle]:
            args = (self.getCenter(), self.getRadius())
        elif self_type in [Polygon]:
            args = self.points.copy()
        elif self_type in [Text]:
            args = (self.anchor.clone(), self.getText())
        elif self_type in [Point]:
            args = (self.x, self.y)
        # Image clone is separate because it does not use config

        other = self.__class__(*args)
        other.config = self.config.copy()
        other._update()
        return other

    def getCenter(self):
        if self.center_point:
            return self.center_point

    def getRadius(self):
        if self.radius:
            return self.radius

    def _fixPoints(self):
        """Since pygame draws rectangles from the top left to the bottom right,
        given points must be adjusted to fit that mold."""
        if self.p1.x < self.p2.x and self.p1.y < self.p2.y:  # p1 top left, p2 bottom right (default)
            self.p1Draw = self.p1.getCenter()
            self.p2Draw = self.p2.getCenter()
        elif self.p1.x > self.p2.x and self.p1.y > self.p2.y:  # p2 top left, p1 bottom right (swap coords)
            self.p1Draw = self.p2.getCenter()
            self.p2Draw = self.p1.getCenter()
        elif self.p1.x < self.p2.x and self.p1.y > self.p2.y:  # p1 bottom left, p2 top right (swap y values)
            self.p1Draw = (self.p1.x, self.p2.y)
            self.p2Draw = (self.p2.x, self.p1.y)
        elif self.p1.x > self.p2.x and self.p1.y < self.p2.y:  # p2 bottom left, p1 top right (swap x values)
            self.p1Draw = (self.p2.x, self.p1.y)
            self.p2Draw = (self.p1.x, self.p2.y)


class Point(GraphicsObject):
    """Drawable Point class. Can be used to build polygons, or drawn to the screen."""
    DEFAULT_CONFIG = {"fill": Color.BLACK, "outline": None}

    def __init__(self, x, y):
        GraphicsObject.__init__(self, ["outline", "fill"])
        self.x = x
        self.y = y
        self.center = (self.x, self.y)

    def __repr__(self):
        return f"Point({self.x}, {self.y})"

    def _draw(self, graphwin):
        """Actual draw call for the class."""
        if self.config["outline"]:
            pygame.draw.circle(graphwin, self.config["outline"], self.center, radius=2)
        if self.config["fill"]:
            pygame.draw.circle(graphwin, self.config["fill"], self.center, radius=1)

    def getX(self):
        return self.x

    def getY(self):
        return self.y

    def getCenter(self):
        return self.center


class Rectangle(GraphicsObject, pygame.Rect):
    """Drawable Rectangle class."""
    # DEV NOTE: Original graphics seems to change fill rect size based on border size??? Doesn't happen in this version
    # but may change to that later if it poses compatibility issues.
    def __init__(self, p1, p2, options=None):
        # p1 and p2 are Point Objects
        if options is None:
            options = ["outline", "width", "fill"]
        GraphicsObject.__init__(self, options)

        self.p1 = p1.clone()
        self.p2 = p2.clone()
        self._fixPoints()

        x, y = self.p1Draw[0], self.p1Draw[1]
        w, h = self.p2Draw[0] - x, self.p2Draw[1] - y
        pygame.Rect.__init__(self, x, y, w, h)

        self.outline_rect = pygame.Rect(self.x - self.config["width"], self.y - self.config["width"],
                                        self.w + 2 * self.config["width"], self.h + 2 * self.config["width"])

        self.center_point = Point(*self.center)

    def __repr__(self):
        return f"Rectangle({self.p1}, {self.p2})"

    def _draw(self, graphwin):
        """Actual draw call for the class."""
        if self.config["outline"]:
            pygame.draw.rect(graphwin, self.config["outline"], self.outline_rect, width=self.config["width"], border_radius=1)
        if self.config["fill"]:
            pygame.draw.rect(graphwin, self.config["fill"], self)

    def _update(self):
        """Internally update the object."""
        self._fixPoints()

        self.x, self.y = self.p1Draw[0], self.p1Draw[1]
        self.w, self.h = self.p2Draw[0] - self.x, self.p2Draw[1] - self.y

        self.outline_rect.x, self.outline_rect.y = self.x - self.config["width"], self.y - self.config["width"]
        self.outline_rect.w, self.outline_rect.h = self.w + 2 * self.config["width"], self.h + 2 * self.config["width"]

        # print(self.x == self.p1Draw[0], self.h + self.p1Draw[1] == self.p2Draw[1])

    def getP1(self):
        return self.p1.clone()

    def getP2(self):
        return self.p2.clone()


class Oval(GraphicsObject):
    def __init__(self, p1, p2, options=None):
        if options is None:
            options = ["outline", "width", "fill"]
        GraphicsObject.__init__(self, options)

        self.p1 = p1.clone()
        self.p2 = p2.clone()
        self._fixPoints()

        self.rect = Rectangle(self.p1, self.p2)
        self.center_point = self.rect.getCenter()

    def __repr__(self):
        return f"Oval({self.p1}, {self.p2})"

    def _draw(self, graphwin):
        """Actual draw call for the class."""
        if self.config["outline"]:
            pygame.draw.ellipse(graphwin, self.config["outline"], self.rect.outline_rect)
        if self.config["fill"]:
            pygame.draw.ellipse(graphwin, self.config["fill"], self.rect)

    def _update(self):
        """Internal update the object."""
        self._fixPoints()

        self.rect.x, self.rect.y = self.p1Draw[0], self.p1Draw[1]
        self.rect.w, self.rect.h = self.p2Draw[0] - self.rect.x, self.p2Draw[1] - self.rect.y

        self.rect.outline_rect.x, self.rect.outline_rect.y = self.rect.x - self.config["width"], self.rect.y - self.config["width"]
        self.rect.outline_rect.w, self.rect.outline_rect.h = self.rect.w + 2 * self.config["width"], self.rect.h + 2 * self.config["width"]


class Circle(Oval):
    def __init__(self, center, radius):
        self.p1 = Point(center.x - radius, center.y - radius)
        self.p2 = Point(center.x + radius, center.y + radius)
        Oval.__init__(self, self.p1, self.p2)

        self.radius = radius

    def __repr__(self):
        return f"Circle({self.getCenter()}, {self.radius})"


# CURRENTLY UNDER CONSTRUCTION
class Line(GraphicsObject):
    def __init__(self, p1, p2, options=None):
        # p1 and p2 are Point Objects
        if options is None:
            options = ["width", "fill", "arrow"]
        GraphicsObject.__init__(self, options)

        self.p1 = p1.clone()
        self.p2 = p2.clone()
        self._fixPoints()

        self.rect = Rectangle(self.p1, self.p2)
        self.center_point = self.rect.getCenter()

        # self.arrow_surf = pygame.Surface((8, 10)).convert()
        # self.arrow_surf.fill((1, 1, 1))
        # points = ((0, 10), (8, 10), (4, 0))
        # pygame.draw.polygon(self.arrow_surf, Color.BLACK, points=points)
        # pygame.draw.line(self.arrow_surf, (1, 1, 1), (2, 9), (6, 9))
        # self.arrow_surf.set_colorkey((1, 1, 1))
        #
        self.angle = int(math.degrees(math.atan((self.p2.x - self.p1.x) / (self.p2.y - self.p1.y))))
        #
        # self.arrow_surf1 = pygame.transform.rotate(self.arrow_surf, self.angle)
        # self.arrow_surf2 = pygame.transform.rotate(self.arrow_surf, self.angle+180)
        # self.arrow_rect = self.arrow_surf.get_rect()

        # arrow_surf = pygame.transform.rotate(arrow_surf, 45)

    def __repr__(self):
        return f"Line({self.p1}, {self.p2})"

    def _draw(self, graphwin):
        """Actual draw call for the class."""
        if self.config["fill"]:
            pygame.draw.line(graphwin, self.config["fill"], self.p1.getCenter(), self.p2.getCenter(), self.config["width"])
        # graphwin.blit(self.arrow_surf, self.p1.getPos())

    def setOutline(self, *color):
        # Mimics behavior of original graphics
        self.setFill(*color)

    def setFill(self, *color):
        GraphicsObject.setFill(self, *color)

        # REDEFINE ARROW SURF WITH NEW COLOR
        # self.arrow_surf = pygame.Surface((8, 10)).convert()
        # self.arrow_surf.fill((1, 1, 1))
        # points = ((0, 10), (8, 10), (4, 0))
        # pygame.draw.polygon(self.arrow_surf, (255, 0, 0), points=points)
        # pygame.draw.line(self.arrow_surf, (1, 1, 1), (2, 9), (6, 9))
        # self.arrow_surf.set_colorkey((1, 1, 1))
        # self.arrow_rect = self.arrow_surf.get_rect()


class Polygon(GraphicsObject):
    def __init__(self, *points):
        # if points passed as a list, extract it
        if len(points) == 1 and type(points[0]) is list:
            points = points[0]
        GraphicsObject.__init__(self, ["outline", "width", "fill"])

        self.points = [p.clone() for p in points]

        self.drawPoints = tuple(p.getCenter() for p in self.points)

    def __repr__(self):
        return "Polygon" + str(tuple(p for p in self.points))

    def _draw(self, graphwin):
        """Actual draw call for the class."""
        # Due to limitations in pygame, any width over 5-ish looks weird because of the lack of auto curving on corners.
        if self.config["fill"]:
            pygame.draw.polygon(graphwin, self.config["fill"], self.drawPoints)
        if self.config["outline"]:
            pygame.draw.polygon(graphwin, self.config["outline"], self.drawPoints, width=self.config["width"])

    def getPoints(self):
        return list(map(Point.clone, self.points))


class Text(GraphicsObject):
    """Display a message on the screen."""
    DEFAULT_CONFIG = {"fill": Color.BLACK,
                      "text": "",
                      "font": {"name": "arial", "size": 12, "bold": False, "italic": False, "custom": False},
                      }

    def __init__(self, p, text):
        GraphicsObject.__init__(self, ["fill", "text", "font"])
        self.anchor = p.clone()
        self.font = pygame.font.SysFont(self.config["font"]["name"], self.config["font"]["size"],
                                        bold=self.config["font"]["bold"], italic=self.config["font"]["italic"])
        self.setText(text)
        self.rect = self.label.get_rect(center=self.anchor.getCenter())

    def __repr__(self):
        return f"Text({self.anchor}, '{self.getText()}')"

    def _draw(self, graphwin):
        """Actual draw call for the class."""
        graphwin.blit(self.label, self.rect)

    def _updateLabel(self):
        """Just updates the label to avoid unnecessarily updating the font. Internal use only."""
        self.label = self.font.render(self.config["text"], True, self.config["fill"])
        self.rect = self.label.get_rect(center=self.anchor.getCenter())

    def _update(self):
        """Updates the Text object fully. Internal use only."""
        if not self.config["font"]["custom"]:
            self.font = pygame.font.SysFont(self.config["font"]["name"], self.config["font"]["size"],
                                            bold=self.config["font"]["bold"], italic=self.config["font"]["italic"])
        else:
            self.font = pygame.font.Font(self.config["font"]["name"], self.config["font"]["size"])
        self._updateLabel()

    def setText(self, text):
        self.config["text"] = text
        self._updateLabel()

    def getText(self):
        return self.config["text"]

    def getAnchor(self):
        return self.anchor.clone()

    def setFace(self, face, custom=False):
        """Sets the font of the Text. For a custom font using a .ttf file, use custom=True.
        To find the list of your system fonts, use the .getFonts static method."""
        self.config["font"]["custom"] = custom

        if not self.config["font"]["custom"]:
            self.config["font"]["name"] = face
            self._update()
        else:
            self.config["font"]["name"] = face
            self.font = pygame.font.Font(face, self.config["font"]["size"])
            self._updateLabel()

    def setSize(self, size):
        self.config["font"]["size"] = size
        self._update()

    def setStyle(self, style):
        """Set if you want bold, italic, or both."""
        if style == "bold":
            self.config["font"]["bold"] = True
            self.config["font"]["italic"] = False
        elif style == "italic":
            self.config["font"]["bold"] = False
            self.config["font"]["italic"] = True
        elif style == "bold italic":
            self.config["font"]["bold"] = True
            self.config["font"]["italic"] = True
        elif style == "normal":
            self.config["font"]["bold"] = False
            self.config["font"]["italic"] = False
        else:
            raise GraphicsError("Please enter a valid style.")

        self._update()

    def setTextColor(self, *color):
        self.setFill(*color)
        self._updateLabel()

    def setOutline(self, *color):
        # Mimics behavior in original graphics
        self.setFill(*color)

    @staticmethod
    def getFonts():
        """Returns a list of all available system fonts on your computer."""
        return pygame.font.get_fonts()


# Probably the biggest challenge because there is no built in text entry to build off of
class Entry(Text):
    def __init__(self, p, width):
        Text.__init__(self, p, "")
        self.width = width * 10

        self.drawRect = self.rect.copy()
        self.drawRect.w = self.width
        self.drawRect.center = self.rect.center

        self.active = False

        self.fill_color = Color.TKINTERGRAY1
        self.outline_color = Color.TKINTERGRAY2
        self.text_color = Color.BLACK

        self.cursor_rect = pygame.Rect(0, 0, self.label.get_height()//10, self.label.get_height())

        self.countdown = 40

    def __repr__(self):
        return f"Entry({self.anchor}, {self.width})"

    def _draw(self, graphwin):
        pygame.draw.rect(graphwin, self.fill_color, self.drawRect)
        pygame.draw.rect(graphwin, self.outline_color, self.drawRect, 2)

        # Blinking Cursor
        if self.active:
            self.cursor_rect.x, self.cursor_rect.y = self.rect.right, self.rect.y
            self.countdown -= 1
            if self.countdown > 0:
                pygame.draw.rect(graphwin, Color.BLACK, self.cursor_rect)
            if self.countdown < -40:
                self.countdown = 40
        else:
            self.countdown = 40

        Text._draw(self, graphwin)
        # pygame.draw.rect(graphwin, Color.RED, self.rect, 2)

    def _update(self):
        Text._update(self)
        self.label = self.font.render(self.getText(), True, self.text_color)
        self.rect = self.label.get_rect(center=self.anchor.getCenter())

        # Resize the box if the text is too long.
        self.drawRect.w = max(self.width, self.label.get_width() + 10)
        self.drawRect.h = self.label.get_height()

        self.cursor_rect.w, self.cursor_rect.h = self.label.get_height()//10, self.label.get_height()

        self.rect.center = self.drawRect.center

    def event_handler(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            if self.drawRect.collidepoint(event.pos):
                self.active = not self.active
            else:
                self.active = False

        if event.type == pygame.KEYDOWN:
            if self.active:
                if event.key == pygame.K_RETURN:
                    value = self.getText()
                    self.setText('')
                    self._update()
                    return value
                elif event.key == pygame.K_BACKSPACE:
                    self.setText(self.getText()[:-1])
                else:
                    self.setText(self.getText() + event.unicode)
                # Re-render the text.
                self._update()

    def setFill(self, *color):
        assign_color(self, "fill_color", *color, to_dict=False)

    def setOutline(self, *color):
        assign_color(self, "outline_color", *color, to_dict=False)

    def setTextColor(self, *color):
        assign_color(self, "text_color", *color, to_dict=False)


class Image(GraphicsObject):
    def __init__(self, p, *pixmap):
        GraphicsObject.__init__(self, [])
        self.anchor = p.clone()

        if len(pixmap) == 1:  # file name provided
            GraphicsError.assertType(pixmap[0], str, "Please enter a valid filename.")
            self.path = pixmap[0]
            self.img = pygame.image.load(self.path)
        else:  # width and height provided
            self.width, self.height = pixmap
            self.img = pygame.Surface((self.width, self.height))

        self.anchor_pos = self.anchor.getCenter()
        self.rect = self.img.get_rect(center=self.anchor_pos)
        self.rel_center = (self.getWidth()//2, self.getHeight()//2)

        self.temp_color = None
        self.scalar = 1

    def __repr__(self):
        return f"Image({self.anchor}, {self.getWidth()}, {self.getHeight()})"

    def _update(self):
        self.anchor_pos = self.anchor.getCenter()
        self.rect = self.img.get_rect(center=self.anchor_pos)
        self.rel_center = (self.getWidth() // 2, self.getHeight() // 2)

    def _draw(self, graphwin):
        graphwin.blit(self.img, self.rect)

    def clone(self):
        """Returns an exact copy of the object."""
        # Location may be moved up to GraphicsObject later
        other = Image(self.anchor.clone(), self.path)
        other.img = self.img
        other._update()
        return other

    def getAnchor(self):
        return self.anchor.clone()

    def getWidth(self):
        """Returns the width of the image in pixels."""
        return self.img.get_width()

    def getHeight(self):
        """Returns the height of the image in pixels."""
        return self.img.get_height()

    def getPixel(self, x, y, include_alpha=False):
        """Returns a tuple with the RGB color values for pixel (x, y). r, g, b are in range(256).
        Naturally in pygame, tuple includes transparency (aka alpha) value, so use include_alpha=True to keep it."""
        # Pixel is measured from the top left
        if x == self.getWidth():
            x -= 1
        if y == self.getHeight():
            y -= 1

        value = self.img.get_at((x, y))
        if not include_alpha:
            value = value[:-1]

        return value

    def setPixel(self, x, y, *color):
        """Sets pixel (x, y) to the given color."""
        assign_color(self, "temp_color", color, to_dict=False)
        self.img.set_at((x, y), self.temp_color)

    def scale(self, factor):
        """Scale the image by the integer factor."""
        GraphicsError.assertType(factor, int, "Please enter an integer scaling factor.")
        self.scalar = factor
        self.img = pygame.transform.scale(self.img, (self.getWidth()*self.scalar, self.getHeight()*self.scalar))
        self._update()

    def save(self, filename):
        """Saves the image to filename. The file format is determined from the filename extension."""
        GraphicsError.assertType(filename, str, "Please enter a valid filename string.")
        pygame.image.save(self.img, filename)


# main() function has been moves to examples.py as a file

if __name__ == '__main__':
    print("This library is not meant to be run directly. Please import it in a different file to use it.")
