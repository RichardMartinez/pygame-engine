from gamegraphics.gamegraphics import *

win = GraphWin()
# win.setBackground("green")
# win.setBackground(Color.SPACEBLUE)

# p = Point(*win.center_pos)
# p.setOutline("tkinterbg")
# p.setFill("red")
# p.draw(win)
#
# p2 = p.clone()
# p2.draw(win)

# o = Oval(Point(10, 30), Point(70, 10))
# o.setFill("red")
# o.setOutline("black")
# o.setWidth(3)
# o.draw(win)
#
# p = o.getCenter()
# p.draw(win)
#
# o2 = o.clone()
# o2.draw(win)

# r = Rectangle(Point(70, 10), Point(10, 30))
# r.setFill("red")
# r.setOutline("black")
# r.setWidth(3)
# r.draw(win)
#
# p = r.getCenter()
# p.draw(win)
#
# r2 = r.clone()
# r2.draw(win)

# player_img = Spritesheet.load_img("player.png", 4)
# formatted_pos = Message.move_pos(win.center_pos, (-player_img.get_width()//2, -player_img.get_height()//2))
# win.blit(player_img, win.center_pos)
#
# p = win.surfCenter(player_img)
# p.draw(win)

# c = Circle(Point(*win.center_pos), 20)
# c.setFill("red")
# c.setWidth(2)
# c.setOutline("black")
# c.draw(win)
#
# print(c.getRadius())
#
# c2 = c.clone()
# c2.draw(win)
#
# p = c.getCenter()
# p.setOutline("peru")
# p.draw(win)

# l = Line(Point(50, 50), Point(10, 10))
# l.setOutline("blue")
# # l.setWidth(10)
# l.draw(win)
#
# p = l.getCenter()
# p.setFill("red")
# p.draw(win)
#
# l2 = l.clone()
# l2.draw(win)

# r = Rectangle(Point(100, 10), Point(10, 100))
# r.draw(win)

# arrow_surf = pygame.Surface((8, 10))
# arrow_surf.fill(Color.TKINTERBG)
# points = ((0, 10), (8, 10), (4, 0))
# pygame.draw.polygon(arrow_surf, Color.BLUE, points=points)
# pygame.draw.line(arrow_surf, Color.TKINTERBG, (2, 9), (6, 9))
# # print(l.angle)
# # arrow_surf = pygame.transform.rotate(arrow_surf, l.angle)
# # swap_color(arrow_surf, Color.BLUE, Color.LIMEGREEN)
# win.blit(arrow_surf, win.center_pos)

# r = Rectangle(Point(10, 50), Point(50, 10))
# # r.setFill("red")
# r.setOutline("red")
# r.setWidth(10)
# r.draw(win)
#
# r2 = r.clone()
# r2.setFill(None)
# r2.draw(win)
#
# r.p1.setOutline("spaceblue")
#
# r.p1.draw(win)
# r.p2.draw(win)
#
# testp = r.getP1()
# testp.draw(win)

# r = Rectangle(Point(0, 160), Point(40, 200))
# r.move(0, -10)
# r.setWidth(8)
# # r.setOutline(None)
# # r.setFill("black")
# r.draw(win)

# r = Rectangle(Point(10, 50), Point(50, 10))
# r.setFill("blue")
# r.setWidth(3)
# r.draw(win)
#
# p = r.getP1()
# p.setFill("red")
# p.draw(win)

# testPoints = [Point(50, 50), Point(70, 50), Point(60, 30)]
# poly = Polygon(testPoints)
# poly.setFill("blue")
# poly.setOutline("brown")
# poly.setWidth(3)
# poly.draw(win)
# print(poly.getPoints())
#
# p2 = poly.clone()
# p2.draw(win)
#
# newpoly = Polygon(Point(100, 180), Point(150, 180), Point(180, 30), Point(30, 30))
# newpoly.draw(win)

# t = Text(Point(*win.center_pos), "This is a test TextBox!")
# t.setText("This should overwrite the Text!")
# t.setFace("8-bit-hud.ttf", custom=True)
# t.setSize(5)
# t.setTextColor("blue")
# t.setStyle("bold italic")
# t.draw(win)
#
# # print(t.config)
#
# t2 = t.clone()
# t2.draw(win)
#
# p = t.anchor
# p.setFill("red")
# p.draw(win)

# testimg = Image(Point(100, 100), "player.png")
# testimg.getPixel(3, 9, include_alpha=True)
# testimg.setPixel(3, 9, Color.LIMEGREEN)
# testimg.getPixel(3, 9)
# testimg.scale(4)
# testimg.draw(win)
#
# p = testimg.anchor
# p.draw(win)
#
# t2 = testimg.clone()
# t2.draw(win)
#
# testimg.save("greenbeltboy.png")

# p = win.plot(10, 10, "red")
# p.setOutline("blue")

# e = Entry(Point(*win.center_pos), 10)
# e.setText("bruh")
# # e.setFace("times new roman")
# e.setSize(25)
# e.draw(win)

# e = Entry(Point(100, 100), 10)
# e.setFill("red")
# e.setTextColor("blue")
# e.setSize(25)
# e.draw(win)

# p = e.getAnchor()
# p.draw(win)

# r = Rectangle(Point(10, 10), Point(50, 50))
# r.draw(win)

# c = Circle(Point(100, 100), 10)
# c.setFill(Color.TKINTERBG)
# c.draw(win)

# e = Entry(Point(*win.center_pos), 10)
# e.draw(win)

c = Circle(Point(*win.center_pos), 10)
c.setFill("red")
c.draw(win)

# print(Color.available())

win.mainloop()